import { Component, OnInit  } from '@angular/core';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { StewardClientService } from 'steward-client';

export class Model{
  upload: any;
}

const URL = 'http://localhost:9000/upload';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent{
  title = 'Tulaa Test';
  selectedFile: File = null;

  model = new Model()

  constructor(private http: HttpClient, private toastr: ToastrService, private steward: StewardClientService<any, any>){

  }

  onFileSelected(event){
    this.selectedFile = <File>event.target.files[0];
  }
  
  onUpload(){
    const fd = new FormData();
    // fd.append('upload', this.selectedFile)
    this.model.upload = this.selectedFile;
    this.steward.postFormDataMultipart('upload', this.model)
    .subscribe(res => {
      if(res.code === 200) {
        this.toastr.success("Success")
      console.log(res);
      alert(res.message);
      } else {
        this.toastr.error(res['message']);
      console.log(res);
      alert(res.message);
      }
    })
  }
}
